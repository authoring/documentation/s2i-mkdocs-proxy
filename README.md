# S2I MkDocs Proxy

In case you are migrating from WebEOS (where URLs look like `https://site-name.web.cern.ch/site-name/...`) to OpenShift (where URLs look like `https://site-name.web.cern.ch/site-name/...`) this proxy will help ease the migration by redirecting the former to the latter. For example, visiting `https://site-name.web.cern.ch/site-name/page.html` or `https://site-name.web.cern.ch/site-name/page/` will redirect the user to `https://site-name.web.cern.ch/page.html` and `https://site-name.web.cern.ch/page/` respectively. At the same time `https://site-name.web.cern.ch/page.html` and `https://site-name.web.cern.ch/page/` will work as expected.

## How it works

1) You are expected to already have an OpenShift project with a functional S2I MkDocs container that serves your documentation at `https://site-name.web.cern.ch/`
2) Create a new GitLab repository for the configuration of your proxy.
    * The only contents of this new GitLab repository will be the `nginx.conf` file that you can copy from here.
    * In the `nginx.conf` file replace `site-name` by your site name and `container-name` by the container name of your actual MkDocs container in your OpenShift project.
3) Delete the existing route in your OpenShift project. This will temporarily make your documentation unavailable.
4) In your OpenShift project go to *Add to project* and then to *Browse Catalog* and select *Nginx HTTP server and a reverse proxy (nginx)*.
    * Enter an *Application Name* of your preference (*proxy* for example).
    * Enter the GitLab repository your just created in the previous step.
    * This will create a new container in your OpenShift project.
5) You're good to go!

You might need to take care of allowing OpenShift to access your private GitLab repository with the Nginx configuration and later securing your recently created new route but that is beyond the scope of this guide.
